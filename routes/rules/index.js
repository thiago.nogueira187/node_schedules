const express = require('express')
const router = express.Router()
const RulesController = require('../../controllers/rules')

/**
 * @api DELETE /api/rules Remove rule
 * @apiParam {String} id
 * @apiExample {curl} Example usage:
 * curl -X DELETE \
 * 'http://localhost:3000/api/rules?id=1ejvnirdb' \
 * -H 'cache-control: no-cache' \
 * -H 'postman-token: f939410c-7319-c664-033c-909f30618bcd'
 */
router.delete('/rules', (req, res) => {
  try {
    const result = RulesController.remove(req.query)

    res.send({ result })
  } catch (error) {
    res.status(500).send({ message: error.message })
  }
})

/**
 * @api POST /api/rules Add rule
 * @apiParam {Array} days
 * @apiParam {String} type
 * @apiParam {Array} [intervals]
 * @apiParam {Array} [intervals[start]]
 * @apiParam {Array} [intervals[end]]
 * @apiExample {curl} Example usage:
 * curl -X POST \
 * http://localhost:3000/api/rules \
 * -H 'cache-control: no-cache' \
 * -H 'content-type: application/json' \
 * -H 'postman-token: cf27c509-e204-a558-5a00-dba55d24fcb5' \
 * -d '{
 *	"type": "specificDay",
 *	"days": ["20-10-2020"],
 *	"intervals": [{ "start": "14:30", "end": "15:00" }, { "start": "15:10", "end": "15:30" }]
 * }'
 */
router.post('/rules', (req, res) => {
  try {
    const result = RulesController.create(req.body)

    res.send(result)
  } catch (error) {
    console.log(error)
    res.status(500).send({ message: error.message })
  }
})

/**
 * @api GET /api/rules list rules
 * @apiExample {curl} Example usage:
 * curl -X GET \
 * http://localhost:3000/api/rules \
 * -H 'cache-control: no-cache' \
 * -H 'postman-token: f2d122b2-4173-f523-11bd-188d3f46a14a'
 */
router.get('/rules', (req, res) => {
  try {
    const result = RulesController.find(req.query)

    res.send(result)
  } catch (error) {
    res.status(500).send({ message: error.message })
  }
})

module.exports = router