const fs = require('fs')
const { join } = require('path')
const { isDirectory, isFile } = require('../lib/folder')
const { basePath } = require('../config')
/**
 * Importar dinamicamente as rotas quando encontrar os arquivos index.js nas pastas
 */
function applyTo (app) {
  for (let child of fs.readdirSync(__dirname)) {
    try {
      let path = join(__dirname, child)
      if (isDirectory(path)) {
        let indexFile = join(path, 'index.js')
        if (isFile(indexFile)) {
          console.log(`Route registered: ${basePath}/${child}`)
          app.use(basePath, require(`./${child}`))
        }
      }
    } catch (e) {
      // erro com o diretorio, igonorar e continuar
      console.error(e)
    }
  }
}

module.exports = applyTo