const express = require('express')
const router = express.Router()
const SchedulesController = require('../../controllers/schedules')

/**
 * @api GET /api/schedules Get all schedules.
 * @apiParam {String} start="Current date"                        Initial date, using format 'DD-MM-YYY'
 * @apiParam {String} end="one day after current date"            End date, using format 'DD-MM-YYY'
 * @apiExample {curl} Example usage:
 * curl -X GET \
 * 'http://localhost:3000/api/schedules?start=05-10-2020&end=21-10-2020' \
 * -H 'cache-control: no-cache' \
 * -H 'postman-token: 3da784f9-ca76-e129-981f-975002b2cfb1'
 */
router.get('/schedules', (req, res) => {
  try {
    const data = SchedulesController.find(req.query)

    res.send(data)
  } catch (error) {
    res.status(500).send({ message: error.message })
  }
})

module.exports = router