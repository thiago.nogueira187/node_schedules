const express = require('express')
const cookieParser = require('cookie-parser')
const routes = require('./routes')

const cors = require('cors')
const app = express()

app.use(cors())

app.use(express.json())

app.use(express.urlencoded({ extended: false }))
app.use(cookieParser())

routes(app)

module.exports = app