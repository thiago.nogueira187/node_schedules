const assert = require('assert')
const SchedulesController = require('../controllers/schedules')

describe('SchedulesController.find', function () {
  it('TSR-006: Get all schedules', function () {
    const data = SchedulesController.find({})

    assert.equal(Array.isArray(data), true)
  })
})