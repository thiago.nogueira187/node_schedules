const assert = require('assert')
const RulesController = require('../controllers/rules')

describe('Rules', function () {
  let rule = {}

  describe('RulesController.create', function () {
    it('TSR-001: Verify type is required', function () {
      try {
        RulesController.create({ type: '', days: [], intervals: [] })
      } catch (error) {
        assert.equal(error.message, 'A001: Type is required.')
      }
    })

    it('TSR-002: Verify days is required', function () {
      try {
        assert.equal(RulesController.create({ type: 'weekly', intervals: [] }), 'A002: Days is required.')
      } catch (error) {
        assert.equal(error.message, 'A002: Days is required.')
      }

      try {
        RulesController.create({ type: 'specificDay', intervals: [] })
      } catch (error) {
        assert.equal(error.message, 'A002: Days is required.')
      }
    })

    it('TSR-003: Week type with invalid day', function () {
      try {
        RulesController.create({ type: 'weekly', days: ['asdsads'], intervals: [] })
      } catch (error) {
        assert.equal(error.message, 'A003: asdsads it is not a valid day.')
      }
    })

    it('TSR-004: invalid type', function () {
      try {
        RulesController.create({ type: 'weeklSSAy', days: ['sun'], intervals: [] })
      } catch (error) {
        assert.equal(error.message, 'A004: Type not valid, type valids specificDay, daily, weekly')
      }
    })

    it('TSR-005: Create rule', function () {
      rule = RulesController.create({ type: 'specificDay', days: ['20-11-2020'], intervals: [{ start: '09:00', end: '12:00' }] })

      if (typeof rule.id === 'string') {
        return assert.ok(true)
      }

      assert.ok(false)
    })
  })

  describe('RulesController.find', function () {
    it('TSR-006: Get all rules', function () {
      const data = RulesController.find()

      assert.equal(Array.isArray(data), true)
    })
  })

  describe('RulesController.remove', function () {
    it('TSR-007: Verify id is required', function () {
      try {
        RulesController.remove({ id: '' })
      } catch (error) {
        assert.equal(error.message, 'A005: Id is required')
      }
    })

    it('TSR-008: Remove rule', function () {
      const result = RulesController.remove({ id: rule.id })

      if (typeof result === 'string') {
        return assert.ok(true)
      }

      assert.ok(false)
    })
  })
})