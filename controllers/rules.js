const db = require('../lib/db')

class RulesController {
  static find () {
    return db.get('rules').value()
  }

  static create (body) {
    const enumTypes = ['specificDay', 'daily', 'weekly']
    const daysOfWeek = ['sun', 'mon', 'tue', 'wed', 'thu', 'fri', 'sat']
    const { days, type, intervals = [] } = body

    if (!type) {
      throw new Error('A001: Type is required.')
    }

    if (!enumTypes.includes(type)) {
      throw new Error(`A004: Type not valid, type valids ${enumTypes.join(', ')}`)
    }

    if (!days && type !== 'daily') {
      throw new Error('A002: Days is required.')
    }

    if (type === 'weekly') {
      const isNotDay = days.find(day => !daysOfWeek.includes(day))

      if (isNotDay) {
        throw new Error(`A003: ${isNotDay} it is not a valid day.`)
      }
    }
  
    const data = {
      id: Date.now().toString(32),
      days,
      type,
      intervals
    }
  
    db.get('rules')
      .push(data)
      .write()
  
    return data
  }

  static remove (query) {
    const { id } = query
  
    if (!id) {
      throw new Error('A005: Id is required')
    }
  
    db.get('rules')
      .remove({ id })
      .write()
  
    return id
  }
}

module.exports = RulesController