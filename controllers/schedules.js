const db = require('../lib/db')
const dayjs = require('dayjs')
const customParseFormat = require('dayjs/plugin/customParseFormat')
dayjs.extend(customParseFormat)

class SchedulesController {
  static find (query) {
    const dates = []
    let { start = dayjs(), end = dayjs(start, 'DD-MM-YYYY') } = query
    const daysOfWeek = ['sat', 'sun', 'mon', 'tue', 'wed', 'thu', 'fri']

    end = dayjs(end, 'DD-MM-YYYY').add(1, 'day')
  
    if (dayjs(start, 'DD-MM-YYYY').isAfter(dayjs(end, 'DD-MM-YYYY'))) {
      throw new Error('B001: Start date greater than end date.')
    }
  
    for (let date = dayjs(start, 'DD-MM-YYYY'); date.isBefore(dayjs(end, 'DD-MM-YYYY')); date = date.add(1, 'day')) {
      const rulesSpecificDay = db.get('rules')
        .find(({ type, days = [] }) => {
          return type === 'specificDay' && days.includes(dayjs(date).format('DD-MM-YYYY') )
        })
        .value() || {}
      const rulesDaily = db.get('rules')
        .find({ type: 'daily' })
        .value() || {}
      const weeklyDaily = db.get('rules')
        .find(({ type, days = [] }) => {
          return type === 'weekly' && days.includes(daysOfWeek[date.day()])
        })
        .value() || {}
      const intervals = rulesSpecificDay.intervals || weeklyDaily.intervals || rulesDaily.intervals

      dates.push({
        day: date.format('DD-MM-YYYY'),
        intervals: intervals || []
      })
    }

    return dates
  }
}

module.exports = SchedulesController