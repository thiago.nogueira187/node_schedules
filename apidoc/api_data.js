define({ "api": [
  {
    "type": "",
    "url": "DELETE",
    "title": "/api/rules Remove rule",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "id",
            "description": ""
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "routes/rules/index.js",
    "group": "/routes/rules/index.js",
    "groupTitle": "/routes/rules/index.js",
    "name": "Delete"
  },
  {
    "type": "",
    "url": "GET",
    "title": "/api/rules list rules",
    "version": "0.0.0",
    "filename": "routes/rules/index.js",
    "group": "/routes/rules/index.js",
    "groupTitle": "/routes/rules/index.js",
    "name": "Get"
  },
  {
    "type": "",
    "url": "POST",
    "title": "/api/rules Add rule",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Array",
            "optional": false,
            "field": "days",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "type",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "Array",
            "optional": true,
            "field": "intervals",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "Array",
            "optional": true,
            "field": "intervals[start]",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "Array",
            "optional": true,
            "field": "intervals[end]",
            "description": ""
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "routes/rules/index.js",
    "group": "/routes/rules/index.js",
    "groupTitle": "/routes/rules/index.js",
    "name": "Post"
  },
  {
    "type": "",
    "url": "GET",
    "title": "/api/schedules Get all schedules.",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "start",
            "defaultValue": "Current date",
            "description": "<p>Initial date, using format 'DD-MM-YYY'</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "end",
            "defaultValue": "one day after current date",
            "description": "<p>End date, using format 'DD-MM-YYY'</p>"
          }
        ]
      }
    },
    "examples": [
      {
        "title": "Example usage:",
        "content": "curl -X GET \\\n'http://localhost:3000/api/schedules?start=05-10-2020&end=21-10-2020' \\\n-H 'cache-control: no-cache' \\\n-H 'postman-token: 3da784f9-ca76-e129-981f-975002b2cfb1'",
        "type": "curl"
      }
    ],
    "version": "0.0.0",
    "filename": "routes/schedules/index.js",
    "group": "/routes/schedules/index.js",
    "groupTitle": "/routes/schedules/index.js",
    "name": "Get"
  }
] });
